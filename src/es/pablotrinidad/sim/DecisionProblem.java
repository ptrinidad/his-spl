package es.pablotrinidad.sim;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DecisionProblem<T> {
	private Collection<T> choices;
	private String decisionTitle;
	
	public DecisionProblem (Collection<T> choices) {
		this.choices = choices;
		decisionTitle = null;
	}

	public DecisionProblem (Collection<T> choices, String decisionTitle) {
		this.choices = choices;
		this.decisionTitle = decisionTitle;
	}
	
	public T makeDecision() {
		int numberOfChoices = choices.size();
		if (decisionTitle != null)
			System.out.println(decisionTitle);
		
		if (numberOfChoices == 0) {
			System.out.println("No existen opciones posibles para tomar una decisión");
			return null;
		} else if (numberOfChoices == 1) {
			T choice = choices.iterator().next();
			System.out.println("Realizando la opción " + choice);
			return choice;
		} else {
			System.out.println("Elija una opción:");
			Map<Integer, T> decisionMap = new HashMap<Integer,T>();
			int i = 1;
			for (T choice: choices) {
				decisionMap.put(i, choice);
				String option = i + ") " + choice.toString();
				System.out.println(option);
				i++;
			}
			int chosenOption = -1;
			while (chosenOption == -1) {
				System.out.print("Elija una opción: ");
				BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
				try {
					String line = bufferRead.readLine();
					chosenOption = Integer.parseInt(line);
					if (chosenOption < 1 || chosenOption >= i) {
						chosenOption = -1;
						System.out.println("Indique una opción de las mostradas");
					}
				} catch (IOException e) {
					System.out.println("Error de entrada. Eligiendo automáticamente la primera opción");
					chosenOption = 1;
				} catch (NumberFormatException e) {
					System.out.println("Indique un número válido");
				}
			}
			return decisionMap.get(chosenOption);
		}
	}
	
	public T makeOptionalDecision() {
		int numberOfChoices = choices.size();
		if (numberOfChoices == 0) {
			System.out.println("No existen opciones posibles para tomar una decisión");
			return null;
		} else {
			System.out.println("Elija una opción:");
			Map<Integer, T> decisionMap = new HashMap<Integer,T>();
			decisionMap.put(0, null);
			System.out.println("0) Ignorar decisión");
			int i = 1;
			for (T choice: choices) {
				decisionMap.put(i, choice);
				String option = i + ") " + choice.toString();
				System.out.println(option);
				i++;
			}
			int chosenOption = -1;
			while (chosenOption == -1) {
				System.out.print("Elija una opción: ");
				BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
				try {
					String line = bufferRead.readLine();
					chosenOption = Integer.parseInt(line);
					if (chosenOption < 0 || chosenOption >= i) {
						chosenOption = -1;
						System.out.println("Indique una opción de las mostradas");
					}
				} catch (IOException e) {
					System.out.println("Error de entrada. Eligiendo automáticamente la primera opción");
					chosenOption = 1;
				} catch (NumberFormatException e) {
					System.out.println("Indique un número válido");
				}
			}
			return decisionMap.get(chosenOption);
		}
	}
}
