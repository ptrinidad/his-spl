package es.usj.master;

import es.us.isa.FAMA.Reasoner.QuestionTrader;
import es.us.isa.FAMA.Reasoner.questions.NumberOfProductsQuestion;
import es.us.isa.FAMA.Reasoner.questions.ValidQuestion;
import es.us.isa.FAMA.models.variabilityModel.VariabilityModel;

public class FAMAMain {

	public static void main(String[] args) {
		//The main class is instantiated 
		QuestionTrader qt = new QuestionTrader();
		
		//A feature model is loaded 
		VariabilityModel fm = qt.openFile("fm-samples/basics/FaMa/fama.fm");
		qt.setVariabilityModel(fm);
		
		////////// VALID QUESTION + NUMBER PRODUCTS QUESTION ///////////
		ValidQuestion vq = (ValidQuestion) qt.createQuestion("Valid");
		qt.ask(vq);
		if (vq.isValid()) {
			NumberOfProductsQuestion npq = (NumberOfProductsQuestion) qt
					.createQuestion("#Products");
			qt.ask(npq);
			System.out.println("The number of products is: "
					+ npq.getNumberOfProducts());
		} else {
			System.out.println("Your feature model is not valid");
		}
	}

}
