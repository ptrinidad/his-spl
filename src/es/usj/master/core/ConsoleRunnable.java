package es.usj.master.core;

public abstract class ConsoleRunnable implements Runnable {
	public ConsoleRunnable () {
	}

	public void run() {
		while (true) {
			mainMenu();
		}
	}

	abstract protected void mainMenu ();
	
	public void start () {
		new Thread(this).start();
	}
}
