package es.usj.master.core;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

import es.usj.master.devices.IDevice;

public class DeviceRegistry {
	private static Collection<IDevice> devices = new HashSet<IDevice>();
	
	public static void registerDevice (IDevice newDevice) {
		devices.add(newDevice);
	}

	public static Collection<IDevice> getAllDevices() {
		return Collections.unmodifiableCollection(devices);
	}
	
	public static Collection<IDevice> getActiveDevices() {
		Collection<IDevice> result = new LinkedList<IDevice>();
		for (IDevice device: devices) {
			if (device.isAvailable())
				result.add(device);
		}
		return result;
	}
}
