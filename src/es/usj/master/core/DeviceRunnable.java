package es.usj.master.core;

public abstract class DeviceRunnable implements Runnable {
	public DeviceRunnable () {
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(1000);
				activate();
			} catch (InterruptedException e) {}
		}
	}

	abstract protected void activate();
	
	public void start () {
		new Thread(this).start();
	}
}
