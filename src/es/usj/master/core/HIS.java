package es.usj.master.core;

import es.usj.master.devices.lights.impl.MockLightControl;
import es.usj.master.devices.supervision.impl.MockSupervisionSystem;
import es.usj.master.devices.systemcontrol.impl.MockControlPanel;

public class HIS {

	public HIS () {
		new MockSupervisionSystem();
		new MockLightControl();
		new MockControlPanel();
		new HISThread().start();
	}
	
	private class HISThread extends DeviceRunnable {
		protected void activate() {
			
		}
	}
}
