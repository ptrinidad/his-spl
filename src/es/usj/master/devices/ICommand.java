package es.usj.master.devices;

public abstract class ICommand {
	public abstract void execute();
	public abstract String commandDescription();
	@Override
	public String toString() {
		return commandDescription();
	}
	
	
}
