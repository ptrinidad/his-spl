package es.usj.master.devices;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import es.usj.master.core.DeviceRegistry;

public abstract class IDevice {
	protected String id;
	protected boolean isAvailable;
	protected Collection<ICommand> availableCommands;

	public IDevice(String id) {
		this.id = id;
		this.isAvailable = true;
		availableCommands = new HashSet<ICommand>();
		restoreCommands();

		DeviceRegistry.registerDevice(this);
	}

	protected void restoreCommands() {
		availableCommands.clear();
		if (isAvailable)
			availableCommands.add(new DeactivateDeviceCommand());
		else
			availableCommands.add(new ActivateDeviceCommand());
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public String getId() {
		return id;
	}
	
	public void activateDevice () {
		isAvailable = true;
		restoreCommands();
	}
	
	public void deactivateDevice () {
		isAvailable = false;
		restoreCommands();
	}

	public Collection<ICommand> getCommands() {
		return Collections.unmodifiableCollection(availableCommands);
	}

	@Override
	public String toString() {
		return getId();
	}

	private class ActivateDeviceCommand extends ICommand {
		@Override
		public void execute() {
			activateDevice();
		}

		@Override
		public String commandDescription() {
			return "Activate device.";
		}
	}

	private class DeactivateDeviceCommand extends ICommand {
		@Override
		public void execute() {
			deactivateDevice();
		}

		@Override
		public String commandDescription() {
			return "Deactivate device.";
		}
	}
}
