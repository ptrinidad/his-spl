package es.usj.master.devices;

public class NullCommand extends ICommand {

	@Override
	public void execute() {}

	@Override
	public String commandDescription() {
		return "Sin acción.";
	}
}
