package es.usj.master.devices.alarm;

import es.usj.master.devices.IDevice;

public abstract class IAlarm extends IDevice {
	protected boolean active;
	
	public IAlarm(String id) {
		super(id);
		active = false;
	}

	public boolean isActive() {
		return active;
	}
	
	public abstract void turnOff();
	public abstract void turnOn();
}
