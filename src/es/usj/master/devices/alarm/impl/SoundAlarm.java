package es.usj.master.devices.alarm.impl;

import es.usj.master.devices.ICommand;
import es.usj.master.devices.alarm.IAlarm;
import es.usj.master.core.DeviceRunnable;

import javax.sound.sampled.*;

public class SoundAlarm extends IAlarm {
	public static float SAMPLE_RATE = 8000f;

	public SoundAlarm(String id) {
		super(id);
		active = false;
		new SoundAlarmRunanble().start();
	}

	public void tone(int hz, int msecs) throws LineUnavailableException {
		tone(hz, msecs, 1.0);
	}

	public void tone(int hz, int msecs, double vol)
			throws LineUnavailableException {
		byte[] buf = new byte[1];
		AudioFormat af = new AudioFormat(SAMPLE_RATE, // sampleRate
				8, // sampleSizeInBits
				1, // channels
				true, // signed
				false); // bigEndian
		SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
		sdl.open(af);
		sdl.start();
		for (int i = 0; i < msecs * 8; i++) {
			double angle = i / (SAMPLE_RATE / hz) * 2.0 * Math.PI;
			buf[0] = (byte) (Math.sin(angle) * 127.0 * vol);
			sdl.write(buf, 0, 1);
		}
		sdl.drain();
		sdl.stop();
		sdl.close();
	}

	private class SoundAlarmRunanble extends DeviceRunnable {

		@Override
		protected void activate() {
			if (isAvailable && active) {
				try {
					tone(1000,100);
					tone(800,100);
				} catch (LineUnavailableException e) {
					System.out.println("Fuego!!!");
				}	
			}
		}

	}

	@Override
	public void turnOff() {
		active = false;
		restoreCommands();
	}

	@Override
	public void turnOn() {
		if (!active) {
			active = true;
			this.availableCommands.add(new TurnOffCommand());
		}
	}

	@Override
	public void activateDevice() {
		super.activateDevice();
		if (active)
			this.availableCommands.add(new TurnOffCommand());
	}

	private class TurnOffCommand extends ICommand {
		@Override
		public void execute() {
			active = false;
			restoreCommands();
		}

		@Override
		public String commandDescription() {
			return "Turn off alarm.";
		}

	}
}
