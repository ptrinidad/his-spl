package es.usj.master.devices.fire.impl;

import java.util.Random;

import es.usj.master.devices.alarm.IAlarm;
import es.usj.master.devices.fire.IFire;

public class MockFireDetection extends IFire {
	private boolean detectedFire;
	private IAlarm alarm;

	public MockFireDetection(String id, IAlarm alarm) {
		super(id);
		detectedFire = false;
		this.alarm = alarm;
		new Thread(new MockFireGenerator()).start();
	}

	private void detectedFire() {
		if (!detectedFire) {
			detectedFire = true;
			alarm.turnOn();
		}
	}

	private void finishedFire() {
		detectedFire = false;
		if (alarm.isAvailable()) {
			alarm.turnOff();
		}
	}

	private class MockFireGenerator implements Runnable {
		Random random;

		MockFireGenerator() {
			random = new Random();
		}

		@Override
		public void run() {
			while (true) {
				try {
					Thread.sleep(100);
					randomFire();
				} catch (InterruptedException e) {
				}
			}
		}

		private void randomFire() {
			if (random.nextInt(100) == 1) {
				if (!detectedFire)
					detectedFire();
				else
					finishedFire();
			}
		}
	}
}
