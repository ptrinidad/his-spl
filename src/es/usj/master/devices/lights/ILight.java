package es.usj.master.devices.lights;

import es.usj.master.devices.IDevice;

public abstract class ILight extends IDevice {
	public ILight(String id) {
		super(id);
	}
	
	public abstract boolean turnOn ();
	public abstract boolean turnOff ();
	public abstract void setPercent (int percentage);
	public abstract void invertState ();

}
