package es.usj.master.devices.lights;

import java.util.Collection;

import es.usj.master.devices.IDevice;
import es.usj.master.devices.ICommand;

public abstract class ILightControl extends IDevice {
	public ILightControl(String id) {
		super(id);
		super.availableCommands.add(new TurnLightsOffCommand());
		super.availableCommands.add(new TurnLightsOnCommand());	
	}
	
	abstract public Collection<ILight> getAvailableLights();
	abstract public void turnAllLightsOn();
	abstract public void turnAllLightsOff();
	
	private class TurnLightsOnCommand extends ICommand {
		public void execute() {
			turnAllLightsOn();
		}
		@Override
		public String commandDescription() {
			return "Turn all lights on.";
		}
	}
	
	private class TurnLightsOffCommand extends ICommand {
		@Override
		public void execute() {
			turnAllLightsOff();
		}
		@Override
		public String commandDescription() {
			return "Turn all lights off.";
		}
	}
}
