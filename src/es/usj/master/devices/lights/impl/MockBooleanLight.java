package es.usj.master.devices.lights.impl;

import es.usj.master.devices.lights.ILight;

public class MockBooleanLight extends ILight {
	private boolean isOn;
	
	public MockBooleanLight (String id) {
		super (id);
		isOn = false;
	}
	
	public MockBooleanLight (boolean state, String id) {
		super (id);
		isOn = state;
	}

	@Override
	public boolean turnOn() {
		isOn = true;
		return true;
	}

	@Override
	public boolean turnOff() {
		isOn = false;
		return true;
	}

	@Override
	public void setPercent(int percentage) {
		if (percentage > 0)
			isOn = true;
		else
			isOn = false;
	}

	@Override
	public void invertState() {
		isOn = !isOn;
	}

}
