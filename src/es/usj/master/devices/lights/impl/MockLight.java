package es.usj.master.devices.lights.impl;

import es.usj.master.devices.lights.ILight;

public class MockLight extends ILight {
	private int percentage;
	
	public MockLight (String id) {
		super (id);
		percentage = 0;
	}
	
	public MockLight (int percentage, String id) {
		super (id);
		if (percentage < 0)
			this.percentage = 0;
		else if (percentage > 100)
			this.percentage = 100;
		else
			this.percentage = percentage;
	}

	@Override
	public boolean turnOn() {
		percentage = 100;
		return true;
	}

	@Override
	public boolean turnOff() {
		percentage = 0;
		return true;
	}

	@Override
	public void invertState() {
		if (percentage > 0)
			percentage = 0;
		else
			percentage = 100;
	}

	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return id + " : " + percentage;
	}

	@Override
	public void setPercent(int percentage) {
		if (percentage < 0)
			this.percentage = 0;
		else if (percentage > 100)
			this.percentage = 100;
		else
			this.percentage = percentage;
	}
}
