package es.usj.master.devices.lights.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import es.usj.master.devices.lights.ILight;
import es.usj.master.devices.lights.ILightControl;

public class MockLightControl extends ILightControl {
	private Set<ILight> availableLights;
	
	public MockLightControl() {
		super("Light Control");
		availableLights = new HashSet<ILight>();
	}
	
	@Override
	public Collection<ILight> getAvailableLights() {
		return Collections.unmodifiableSet(availableLights);
	}

	@Override
	public void turnAllLightsOn() {
		for (ILight light: availableLights) {
			light.turnOn();
		}
	}

	@Override
	public void turnAllLightsOff() {
		for (ILight light: availableLights) {
			light.turnOff();
		}
	}

}
