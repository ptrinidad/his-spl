package es.usj.master.devices.supervision.impl;

import es.usj.master.devices.alarm.IAlarm;
import es.usj.master.devices.alarm.impl.SoundAlarm;
import es.usj.master.devices.fire.IFire;
import es.usj.master.devices.fire.impl.MockFireDetection;
import es.usj.master.devices.supervision.ISupervisionSystem;

public class MockSupervisionSystem extends ISupervisionSystem {
	IFire fireDetection;
	IAlarm alarm;
	
	public MockSupervisionSystem() {
		super ("Supervision System");
		alarm = new SoundAlarm("Sound Alarm");
		fireDetection = new MockFireDetection("Fire detector", alarm);
	}
}
