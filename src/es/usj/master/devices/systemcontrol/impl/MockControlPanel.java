package es.usj.master.devices.systemcontrol.impl;

import java.util.Collection;
import java.util.LinkedList;

import es.pablotrinidad.sim.DecisionProblem;
import es.usj.master.core.ConsoleRunnable;
import es.usj.master.core.DeviceRegistry;
import es.usj.master.devices.ICommand;
import es.usj.master.devices.IDevice;
import es.usj.master.devices.NullCommand;
import es.usj.master.devices.systemcontrol.IControlPanel;

public class MockControlPanel extends IControlPanel {
	
	public MockControlPanel() {
		super("Control Panel");
		new Thread(new ControlPanelConsole()).start();
	}
	
	private class ControlPanelConsole extends ConsoleRunnable {

		@Override
		protected void mainMenu() {
			printStatus();
			System.out.println("Menú principal");
			System.out.println("--------------");
			DecisionProblem<IDevice> decision = new DecisionProblem<IDevice>(DeviceRegistry.getAllDevices());
			IDevice device = decision.makeDecision();
			Collection<ICommand> commandsSet = new LinkedList<ICommand>();
			commandsSet.addAll(device.getCommands());
			commandsSet.add(new NullCommand());
			DecisionProblem<ICommand> commandProblem = new DecisionProblem<ICommand>(commandsSet);
			ICommand command = commandProblem.makeDecision();
			command.execute();
		}

		private void printStatus() {
			System.out.print("Status: ");
			System.out.println(DeviceRegistry.getAllDevices());
			
		}
	}

}
